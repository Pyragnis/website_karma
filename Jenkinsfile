pipeline {
    agent any

    environment {
        DOCKER_BIN = 'docker'
        DOCKER_IMAGE = 'pyragnis91/website_karma'
        IMAGE_TAG = 'latest'
        REGISTRY = 'https://index.docker.io/v1/'
        REGISTRY_CREDENTIALS_ID = 'dockerhub-login' // Use the credentials ID directly
        HEROKU_APP_NAME = 'websitekarma2'
    }

    stages {
        stage('Build image') {
            steps {
                script {
                    // Building the Docker image with the AMD64 platform support
                    bat "${DOCKER_BIN} build -t ${DOCKER_IMAGE}:${IMAGE_TAG} ."
                }
            }
        }

        stage('Test image') {
            steps {
                script {
                    // Test the Docker image by running a container and checking for expected content
                    bat "${DOCKER_BIN} rm -f web || true"
                    bat "${DOCKER_BIN} run --name web -d -p 5002:80 ${DOCKER_IMAGE}:${IMAGE_TAG}"
                    bat "ping 127.0.0.1 -n 11 > nul" // Pause for 10 seconds
                    bat "curl -s http://localhost:5002 | findstr /C:\"<h1>Nike New <br>Collection!</h1>\" > nul"
                }
            }
        }

        stage('Push image to Docker Hub') {
            steps {
                script {
                    // Pushing the Docker image to Docker Hub
                    withCredentials([usernamePassword(credentialsId: REGISTRY_CREDENTIALS_ID, usernameVariable: 'DOCKERHUB_USER', passwordVariable: 'DOCKERHUB_PASS')]) {
                        docker.withRegistry('https://docker.io', 'dockerhub-login') {
                            def dockerImage = docker.image("${DOCKER_IMAGE}:${IMAGE_TAG}")
                            dockerImage.push()
                        }
                    }
                }
            }
        }

        stage('Deploy to Heroku') {
            steps {
                script {
                    // Deploying the Docker image to Heroku
                    withCredentials([string(credentialsId: 'heroku_api_key', variable: 'HEROKU_API_KEY')]) {
                        bat """
                        ${DOCKER_BIN} tag ${DOCKER_IMAGE}:${IMAGE_TAG} registry.heroku.com/${HEROKU_APP_NAME}/web
                        echo $HEROKU_API_KEY | ${DOCKER_BIN} login --username=_ --password-stdin registry.heroku.com
                        ${DOCKER_BIN} push registry.heroku.com/${HEROKU_APP_NAME}/web
                        heroku container:release web --app ${HEROKU_APP_NAME}
                        """
                    }
                }
            }
        }

        stage('Check Deployment') {
            steps {
                script {
                    // Checking if the deployed application is live and accessible
                    bat "curl -s https://${HEROKU_APP_NAME}.herokuapp.com"
                }
            }
        }
    }

    post {
        always {
            echo "Sending build completion email..."
            emailext(
                to: 'enzosabbatorsi@outlook.fr',
                subject: "Build ${currentBuild.fullDisplayName} completed",
                body: """<html><body>
                         <p>The build of <strong>${env.JOB_NAME}</strong> build number <strong>${env.BUILD_NUMBER}</strong> was completed.</p>
                         <p>Status: <strong>${currentBuild.currentResult}</strong></p>
                         <p>View build details at <a href='${BUILD_URL}'>${BUILD_URL}</a>.</p>
                         </body></html>""",
                mimeType: 'text/html'
            )
        }
        success {
            echo 'Build and deployment were successful!'
        }
        failure {
            echo 'Build or deployment failed.'
        }
    }
}


