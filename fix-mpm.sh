#!/bin/bash
# Disable unwanted MPMs
a2dismod mpm_event mpm_worker
# Enable desired MPM
a2enmod mpm_prefork
# Start Apache in the foreground
exec apache2-foreground